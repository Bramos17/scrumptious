from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from recipes.views import (
    recipe_list,
    show_recipe,
    create_recipe,
    edit_recipe,
    uploadok,
)


urlpatterns = [
    path("mine/", recipe_list, name="my_recipe_list"),
    path("recipes/", recipe_list, name="recipe_list"),
    path("recipes/<int:id>/", show_recipe, name="show_recipe"),
    path("recipes/create/", create_recipe, name="create_recipe"),
    path("recipes/<int:id>/edit/", edit_recipe, name="edit_recipe"),
    path("image_upload/", create_recipe, name="create_recipe"),
    path("success/", uploadok, name="success"),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
